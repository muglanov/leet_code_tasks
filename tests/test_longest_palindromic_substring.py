# coding: utf-8

from unittest import TestCase, main

from tasks.longest_palindromic_substring import Solution, is_palindrome

class LongestPalindromicSubstringTestCase(TestCase):
    def test_longestPalindrome(self):
        input_data = 'babad'
        output = 'bab'
        self.assertEqual(Solution.longestPalindrome(input_data), output)
        input_data = 'cbbd'
        output = 'bb'
        self.assertEqual(Solution.longestPalindrome(input_data), output)
        input_data = 'a'
        output = 'a'
        self.assertEqual(Solution.longestPalindrome(input_data), output)
        input_data = 'ac'
        output = 'a'
        self.assertEqual(Solution.longestPalindrome(input_data), output)
        input_data = 'babaddtattarrattatddetartrateedredividerb'
        output = 'ddtattarrattatdd'
        self.assertEqual(Solution.longestPalindrome(input_data), output)

    def test_is_palindrome(self):
        input_data = 'bb'
        res = True
        self.assertEqual(is_palindrome(input_data), res)
        input_data = 'bab'
        res = True
        self.assertEqual(is_palindrome(input_data), res)
        input_data = 'baba'
        res = False
        self.assertEqual(is_palindrome(input_data), res)
        input_data = 'baab'
        res = True
        self.assertEqual(is_palindrome(input_data), res)
        input_data = 'ababbaba'
        res = True
        self.assertEqual(is_palindrome(input_data), res)
        input_data = 'ababababa'
        res = True
        self.assertEqual(is_palindrome(input_data), res)
        input_data = 'abcdefg'
        res = False
        self.assertEqual(is_palindrome(input_data), res)


if __name__ == '__main__':
    main()
