# coding:utf-8

import unittest
from tasks.longest_common_prefix import Solution


class MyTestCase(unittest.TestCase):

    def test_prefix(self):
        input_lst = ['flower','flow','flight']
        self.assertEqual(Solution.longestCommonPrefix(input_lst), 'fl')

        input_lst = ['dog','racecar','car']
        self.assertEqual(Solution.longestCommonPrefix(input_lst), '')


if __name__ == '__main__':
    unittest.main()
