# coding:utf-8

import unittest

from tasks.three_sum import Solution


class ThreeSumTestCase(unittest.TestCase):
    def test_easy_case(self):
        input_data = [-1, 0, 1, 2, -1, -4]
        result_data = [
            sorted([-1, 0, 1]),
            sorted([-1, -1, 2])
        ]
        self.assertEqual(Solution.threeSum(input_data), result_data)

    def test_wrong_case_1(self):
        input_data = [3, -2, 1, 0]
        result_data = []
        self.assertEqual(Solution.threeSum(input_data), result_data)


if __name__ == '__main__':
    unittest.main()
