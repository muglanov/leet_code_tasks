# coding: utf-8

import unittest

from tasks.palindrome_number import Solution


class MyTestCase(unittest.TestCase):
    def test_palindrome_number(self):
        self.assertEqual(Solution.isPalindrome(121), True)
        self.assertEqual(Solution.isPalindrome(-121), False)
        self.assertEqual(Solution.isPalindrome(10), False)


if __name__ == '__main__':
    unittest.main()
