# coding: utf-8

import unittest

from tasks.zigzag_conversion import Solution


class MyTestCase(unittest.TestCase):
    def test_zigzag_conversion(self):
        input_str = 'PAYPALISHIRING'
        row_count = 3
        output = 'PAHNAPLSIIGYIR'
        self.assertEqual(Solution.convert(input_str, row_count), output)
        input_str = 'PAYPALISHIRING'
        row_count = 4
        output = 'PINALSIGYAHRPI'
        self.assertEqual(Solution.convert(input_str, row_count), output)

        input_str = 'AB'
        row_count = 1
        output = 'AB'
        self.assertEqual(Solution.convert(input_str, row_count), output)

    def test_empty_string(self):
        input_str = ''
        row_count = 1
        output = ''
        self.assertEqual(Solution.convert(input_str, row_count), output)


if __name__ == '__main__':
    unittest.main()
