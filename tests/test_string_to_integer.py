# coding: utf-8

import unittest

from tasks.string_to_integer import Solution


class MyTestCase(unittest.TestCase):
    def test_string_to_integer(self):
        self.assertEqual(Solution.myAtoi('42'), 42)
        self.assertEqual(Solution.myAtoi('    -42'), -42)
        self.assertEqual(Solution.myAtoi('4193 with words'), 4193)
        self.assertEqual(Solution.myAtoi('words and 987'), 0)
        self.assertEqual(Solution.myAtoi('  0000000000012345678'), 12345678)
        self.assertEqual(Solution.myAtoi('2147483647'), 2147483647)

    def test_empty_string_to_integer(self):
        self.assertEqual(Solution.myAtoi(''), 0)

    def test_only_minus_string_to_integer(self):
        self.assertEqual(Solution.myAtoi('-'), 0)

    def test_string_to_integer_with_plus(self):
        self.assertEqual(Solution.myAtoi('+1'), 1)

    def test_string_to_integer_with_plus_minus(self):
        self.assertEqual(Solution.myAtoi('+-2'), 0)


if __name__ == '__main__':
    unittest.main()
