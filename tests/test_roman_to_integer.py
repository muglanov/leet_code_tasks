# coding:utf-8

import unittest

from tasks.roman_to_integer import Solution


class MyTestCase(unittest.TestCase):

    def test_digits(self):
        self.assertEqual(Solution.romanToInt('I'), 1)
        self.assertEqual(Solution.romanToInt('II'), 2)
        self.assertEqual(Solution.romanToInt('III'), 3)
        self.assertEqual(Solution.romanToInt('IV'), 4)
        self.assertEqual(Solution.romanToInt('V'), 5)
        self.assertEqual(Solution.romanToInt('VI'), 6)
        self.assertEqual(Solution.romanToInt('VII'), 7)
        self.assertEqual(Solution.romanToInt('VIII'), 8)
        self.assertEqual(Solution.romanToInt('IX'), 9)

    def test__complex_numbers(self):
        self.assertEqual(Solution.romanToInt('LVIII'), 58)
        self.assertEqual(Solution.romanToInt('MCMXCIV'), 1994)

if __name__ == '__main__':
    unittest.main()
