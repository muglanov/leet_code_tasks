# coding:utf-8

from unittest import TestCase

from tasks.longest_substring_without_repeating_characters import Solution


class LongestSubstringWithoutRepeatingCharactersTestCase(TestCase):

    def test_length_of_longest_substring(self):
        self.assertEqual(Solution.lengthOfLongestSubstring('abcabcbb'), 3)
        self.assertEqual(Solution.lengthOfLongestSubstring('bbbbb'), 1)
        self.assertEqual(Solution.lengthOfLongestSubstring('pwwkew'), 3)
        self.assertEqual(Solution.lengthOfLongestSubstring('dvdf'), 3)

    def test_length_of_longest_substring_with_empty_string(self):
        self.assertEqual(Solution.lengthOfLongestSubstring(' '), 1)
