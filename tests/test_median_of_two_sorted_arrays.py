# coding: utf-8

from unittest import TestCase, main

from tasks.median_of_two_sorted_arrays import Solution


class MedianOfTwoSortedArraysTestCase(TestCase):
    def test_findMedianSortedArrays(self):
        nums1 = [1, 3]
        nums2 = [2]
        res = 2
        self.assertEqual(Solution.findMedianSortedArrays(nums1, nums2), res)
        nums1 = [1, 2]
        nums2 = [3, 4]
        res = 2.5
        self.assertEqual(Solution.findMedianSortedArrays(nums1, nums2), res)
        nums1 = [1, 2, 5, 9, 13, 15]
        nums2 = [3, 4, 6, 7, 8, 10, 11, 12, 14]
        res = 8
        self.assertEqual(Solution.findMedianSortedArrays(nums1, nums2), res)


if __name__ == '__main__':
    main()
