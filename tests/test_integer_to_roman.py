# coding: utf-8

import unittest

from tasks.integer_to_roman import Solution


class MyTestCase(unittest.TestCase):
    def test_from_one_to_ten(self):
        self.assertEqual(Solution.intToRoman(1), 'I')
        self.assertEqual(Solution.intToRoman(2), 'II')
        self.assertEqual(Solution.intToRoman(3), 'III')
        self.assertEqual(Solution.intToRoman(4), 'IV')
        self.assertEqual(Solution.intToRoman(5), 'V')
        self.assertEqual(Solution.intToRoman(6), 'VI')
        self.assertEqual(Solution.intToRoman(7), 'VII')
        self.assertEqual(Solution.intToRoman(8), 'VIII')
        self.assertEqual(Solution.intToRoman(9), 'IX')
        self.assertEqual(Solution.intToRoman(10), 'X')

    def test_double_digit(self):
        self.assertEqual(Solution.intToRoman(58), 'LVIII')

    # def test_three_digit(self):
    #     pass

    def test_four_digit(self):
        self.assertEqual(Solution.intToRoman(1994), 'MCMXCIV')


if __name__ == '__main__':
    unittest.main()
