# coding:utf-8

from unittest import TestCase

from tasks.add_two_numbers import Solution, ListNode, get_list_node_by_number, get_number_by_list_node, \
    get_number_by_list_node_recursive, get_list_node_by_number_recursive


# TODO: дописать тесты для создания нодов


class AddTwoNumbersTestCase(TestCase):

    # def test_create_list_node(self):
    #     pass
    #     self.assertEqual(1, 2)

    def test_add_two_numbers_1(self):
        ln1 = get_list_node_by_number(342)
        ln2 = get_list_node_by_number(465)
        res_ln = Solution.addTwoNumbers(ln1, ln2)
        res_num = get_number_by_list_node(res_ln)
        self.assertEqual(res_num, 807)

    def test_add_two_numbers_2(self):
        ln1 = get_list_node_by_number(9)
        ln2 = get_list_node_by_number(999999991)
        res_ln = Solution.addTwoNumbers(ln1, ln2)
        res_num = get_number_by_list_node(res_ln)
        self.assertEqual(res_num, 1000000000)

    def test_get_number_by_list_node_recursive(self):
        ln = get_list_node_by_number(342)
        res = get_number_by_list_node(ln)
        res_recursive = get_number_by_list_node_recursive(ln)
        self.assertEqual(res, res_recursive)

    def test_get_list_node_by_number_recursive(self):
        ln1 = get_list_node_by_number(342)
        ln2 = get_list_node_by_number_recursive(342)
        res1 = get_number_by_list_node_recursive(ln1)
        res2 = get_number_by_list_node_recursive(ln2)
        self.assertEqual(res1, res2)
