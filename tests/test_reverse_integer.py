# coding: utf-8

import unittest

from tasks.reverse_integer import Solution


class MyTestCase(unittest.TestCase):
    def test_reverse_integer(self):
        self.assertEqual(Solution.reverse(123), 321)
        self.assertEqual(Solution.reverse(-123), -321)
        self.assertEqual(Solution.reverse(120), 21)
        self.assertEqual(Solution.reverse(0), 0)
        self.assertEqual(Solution.reverse(1534236469), 0)


if __name__ == '__main__':
    unittest.main()