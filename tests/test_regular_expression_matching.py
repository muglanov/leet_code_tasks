# coding: utf-8

import unittest

from tasks.regular_expression_matching import Solution


class MyTestCase(unittest.TestCase):
    def test_regular_expression_matching(self):
        input_str = 'aa'
        pattern = 'a'
        self.assertEqual(Solution.isMatch(input_str, pattern), False)

        input_str = 'aa'
        pattern = 'a*'
        self.assertEqual(Solution.isMatch(input_str, pattern), True)

        input_str = 'ab'
        pattern = '.*'
        self.assertEqual(Solution.isMatch(input_str, pattern), True)

        input_str = 'aab'
        pattern = 'c*a*b'
        self.assertEqual(Solution.isMatch(input_str, pattern), True)

        input_str = 'mississippi'
        pattern = 'mis*is*p*.'
        self.assertEqual(Solution.isMatch(input_str, pattern), False)


if __name__ == '__main__':
    unittest.main()
