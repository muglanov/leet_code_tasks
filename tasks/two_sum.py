# coding:utf-8

from typing import List


class Solution:
    @staticmethod
    def twoSum(nums: List[int], target: int) -> List[int]:
        elems_dict = {}
        for i in range(len(nums)):
            elems_dict[nums[i]] = i
        for i in range(len(nums)):
            complement = target - nums[i]
            if complement in elems_dict and elems_dict[complement] != i:
                return [i, elems_dict[complement]]


if __name__ == '__main__':
    # print(Solution.twoSum([2, 7, 11, 15], 9))
    print(Solution.twoSum([3, 2, 4], 6))
    print(Solution.twoSum([3, 3], 6))
