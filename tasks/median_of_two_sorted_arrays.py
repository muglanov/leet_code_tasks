# coding: utf-8

from typing import List


class Solution:
    @staticmethod
    def findMedianSortedArrays(nums1: List[int], nums2: List[int]) -> float:
        i = j = 0
        res_list = []
        while i < len(nums1) and j < len(nums2):
            if nums1[i] < nums2[j]:
                res_list.append(nums1[i])
                i += 1
            else:
                res_list.append(nums2[j])
                j += 1
        res_list.extend(nums1[i:])
        res_list.extend(nums2[j:])
        div, mod = divmod(len(res_list), 2)
        if mod:
            res = res_list[div]
        else:
            res = (res_list[div - 1] + res_list[div]) / 2.0
        return res
