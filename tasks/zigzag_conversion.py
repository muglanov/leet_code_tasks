# coding: utf-8


class Solution:
    @staticmethod
    def convert(s: str, numRows: int) -> str:
        if len(s) < 2 or numRows == 1:
            return s
        zigzag_matrix = []
        for i in range(numRows):
            zigzag_matrix.append({})
        j = k = 0
        zigzag_matrix[0][0] = s[k]
        k += 1
        while k < len(s):
            for i in range(1, numRows):
                if k == len(s):
                    break
                zigzag_matrix[i][j] = s[k]
                k += 1
            for i in range(numRows - 2, -1, -1):
                if k == len(s):
                    break
                j += 1
                zigzag_matrix[i][j] = s[k]
                k += 1
        res = ''
        for row in zigzag_matrix:
            index_list = sorted(row)
            for num in index_list:
                res += row[num]
        return res
