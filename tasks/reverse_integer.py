# coding: utf-8


class Solution:
    @staticmethod
    def reverse(x: int) -> int:
        x_str = str(x)
        if len(x_str) < 2:
            return x
        sign = 1
        if x_str[0] == '-':
            sign = -1
            x_str = x_str[1:]
        while x_str[-1] == '0':
            x_str = x_str[:-1]
        result = x_str[::-1]
        int_result = sign * int(result)
        return int_result if ( - 2**31 < int_result < 2**31 - 1) else 0
