# coding: utf-8

from re import match


class Solution:
    @staticmethod
    def myAtoi(input_str: str) -> int:
        input_str = input_str.strip()
        pattern = r'^([-,+]?\d+).*$'
        output = match(pattern, input_str)
        if not output:
            return 0
        res_int = int(output.group(1))
        left_border = -2**31
        right_border = 2**31 - 1
        if left_border <= res_int <= right_border:
            return res_int
        elif left_border > res_int:
            return left_border
        elif res_int > right_border:
            return right_border
