# coding: utf-8

from re import fullmatch


class Solution:
    @staticmethod
    def isMatch(s: str, p: str) -> bool:
        sr = fullmatch(p, s)
        return bool(sr)
