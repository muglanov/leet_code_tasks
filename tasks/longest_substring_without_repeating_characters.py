# coding:utf-8


class Solution:
    @staticmethod
    def lengthOfLongestSubstring(s: str) -> int:
        letter_dict = {}
        max_count = 0
        i = 0
        begin_of_window = -1
        while i < len(s):
            if s[i] in letter_dict:
                max_count = max(max_count, len(letter_dict))
                prev_same_char_num = letter_dict[s[i]]
                for j in range(begin_of_window + 1, prev_same_char_num + 1):
                    del letter_dict[s[j]]
                begin_of_window = prev_same_char_num
            letter_dict[s[i]] = i
            i += 1
        return max(max_count, len(letter_dict))
