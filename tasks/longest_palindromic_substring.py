# coding: utf-8


def is_palindrome(s: str) -> bool:
    return s == s[::-1]


class Solution:
    @staticmethod
    def longestPalindrome(s: str) -> str:
        n = len(s)
        while n > 1:
            for j in range(len(s) - n + 1):
                if is_palindrome(s[j:n + j]):
                    return s[j:n + j]
            n -= 1
        return s[0] if s else ''
