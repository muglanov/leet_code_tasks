# coding: utf-8

from typing import List


class Solution:
    @staticmethod
    def maxArea(height: List[int]) -> int:
        left = 0
        right = len(height) - 1
        max_water_capacity = 0
        while left != right:
            max_water_capacity = max(max_water_capacity, min(height[left], height[right]) * (right - left))
            if height[left] < height[right]:
                left += 1
            else:
                right -= 1
        return max_water_capacity
