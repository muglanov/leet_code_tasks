# coding: utf-8


class Solution:
    @staticmethod
    def isPalindrome(x: int) -> bool:
        if x < 0:
            return False
        digit_list = []
        div_mod = divmod(x, 10)
        digit_list.append(div_mod[1])
        while div_mod[0] != 0:
            div_mod = divmod(div_mod[0], 10)
            digit_list.append(div_mod[1])
        return digit_list == digit_list[::-1]
