# coding: utf-8


def int_to_romain(num: int) -> str:
    result = ''
    if num < 10:
        one = 'I'
        five = 'V'
        ten = 'X'
        first_digit = num
    elif num < 100:
        one = 'X'
        five = 'L'
        ten = 'C'
        first_digit = num // 10
    elif num < 1000:
        one = 'C'
        five = 'D'
        ten = 'M'
        first_digit = num // 100
    else:
        first_digit = num // 1000
        for i in range(first_digit):
            result += 'M'
        return result
    if first_digit < 4:
        for i in range(first_digit):
            result += one
    elif first_digit == 4:
        result = one + five
    elif first_digit < 9:
        result = five
        for i in range(first_digit - 5):
            result += one
    elif first_digit == 9:
        result = one + ten
    return result


class Solution:
    @staticmethod
    def intToRoman(num: int) -> str:
        numbers_list = []
        div_mod = divmod(num, 10)
        numbers_list.append(div_mod[1])
        ten_pow = 1
        while div_mod[0] != 0:
            div_mod = divmod(div_mod[0], 10)
            numbers_list.append(div_mod[1] * (10 ** ten_pow))
            ten_pow += 1
        result = ''.join(list(map(int_to_romain, numbers_list))[::-1])
        return result
