# coding:utf-8

from typing import List


class Solution:
    @staticmethod
    def threeSum(nums: List[int]) -> List[List[int]]:
        result_list = []
        for i in range(len(nums) - 2):
            for j in range(i + 1, len(nums) - 1):
                elem_to_find = -1 * (nums[i] + nums[j])
                if elem_to_find in nums[j + 1:]:
                    item = sorted([nums[i], nums[j], elem_to_find])
                    if item not in result_list:
                        result_list.append(item)
                        continue
        return result_list
