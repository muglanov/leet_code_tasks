# coding: utf-8

from typing import List


class Solution:
    @staticmethod
    def longestCommonPrefix(strs: List[str]) -> str:
        if len(strs) < 1:
            return ''
        elif len(strs) == 1:
            return strs[0]
        prefix, another = (strs[0], strs[1]) if len(strs[0]) < len(strs[1]) else (strs[1], strs[0])
        while len(prefix) > 0:
            check_failed = False
            for word in strs:
                if not word.startswith(prefix):
                    check_failed = True
                    prefix = prefix[:-1]
                    break
            if not check_failed:
                break
        return prefix
