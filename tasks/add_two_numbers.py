# coding:utf-8

from copy import copy


class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


def get_number_by_list_node(ln: ListNode) -> int:
    copy_ln = copy(ln)
    number = copy_ln.val
    digit_power = 1
    while copy_ln.next is not None:
        copy_ln = copy_ln.next
        number += pow(10, digit_power) * copy_ln.val
        digit_power += 1
    return number


def get_number_by_list_node_recursive(ln: ListNode, pow_number: int = 0) -> int:
    if ln.next is None:
        return ln.val * pow(10, pow_number)
    return ln.val * pow(10, pow_number) + get_number_by_list_node_recursive(ln.next, pow_number + 1)


def get_list_node_by_number(numb: int) -> ListNode:
    res = divmod(numb, 10)
    result_ln = ListNode(res[1])
    ln = result_ln
    while res[0] != 0:
        res = divmod(res[0], 10)
        ln.next = ListNode(res[1])
        ln = ln.next
    return result_ln


def get_list_node_by_number_recursive(numb: int) -> ListNode:
    res = divmod(numb, 10)
    res_ln = ListNode(res[1])
    if res[0] != 0:
        res_ln.next = get_list_node_by_number_recursive(res[0])
    return res_ln


class Solution:
    @staticmethod
    def addTwoNumbers(l1: ListNode, l2: ListNode) -> ListNode:
        # number_1 = get_number_by_list_node(l1)
        # number_2 = get_number_by_list_node(l2)
        number_1 = get_number_by_list_node_recursive(l1)
        number_2 = get_number_by_list_node_recursive(l2)
        # return get_list_node_by_number(number_1 + number_2)
        return get_list_node_by_number_recursive(number_1 + number_2)