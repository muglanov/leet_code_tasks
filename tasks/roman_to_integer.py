# coding:utf-8

simple_roman_dict = {
    'I': 1,
    'V': 5,
    'X': 10,
    'L': 50,
    'C': 100,
    'D': 500,
    'M': 1000
}

complex_roman_dict = {
    'IV': 4,
    'IX': 9,
    'XL': 40,
    'XC': 90,
    'CD': 400,
    'CM': 900
}


class Solution:
    @staticmethod
    def romanToInt(s: str) -> int:
        result = 0
        for number in complex_roman_dict:
            if number in s:
                result += complex_roman_dict[number]
                s = s.replace(number, '')
        for number in s:
            result += simple_roman_dict[number]
        return result
